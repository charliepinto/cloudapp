---
layout: markdown_page
title: "Outbound SDR"
---
---


# Outbound SDR Handbook<a name="outbound"></a>
{:.no_toc}

----

## On this page
{:.no_toc}

- TOC
{:toc}

## Your Role

As an Outbound Sales Development Representative (SDR), you will be responsible for one of the most difficult and important parts of growing a business: outbound prospecting. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating sales accepted opportunities, within our large and strategic account segments.

## Working with Account Executives

Account Executives (AEs) receive support from the Outbound SDR team. Meeting cadence consist of the following:

- **First kick-off meeting**: Time - 1hour; Discuss strategy, accounts and schedules
- **Monthly Recurring Strategy Meeting**: Time - 1hour; Evalutate strategy and opportunities
- **Weekly (or biweekly) Status Meeting**: Time - 30 minutes; Discuss initial meetings and opportunities

Additional ad-hoc meetings may be scheduled in addition to the above.

AEs have direct impact on the Outbound SDR's success. Forming a strong relationship with the AEs and SALs you support, and acting as a single integrated account team will play a crucial role in obtaining quota each month. The AEs and SALs you work with will know a lot about the accounts you are prospecting into, so be sure to be in regular communication to ensure you are as effective as possible.

## Working with an Account Research Specialist

The role of Account Research Specialist supports SDRs by identifying strategic initiatives within a large or strategic sales segment account, and key contacts within the account, to help the SDR team stay focused on prospecting and improve SDR productivity. SDRs can request support in researching accounts that have been identified in coordination with the Outbound SDR Team Lead and the respective Account Executive.

To submit a request, open an issue in the Marketing Project with the following criteria:
*  "Research Request - {Account Name}" in the title field
*  Research requirements in the description field
*  A link to the account in SFDC in the description field
*  Assignee as yourself and the Account Research Specialist
*  Issue marked as confidential as issues will contain information sourced through paid services

SDRs should be as specific as possible in identifying what information they are looking for. Examples of what a SDR could request, but not limited to, are:

*  An account's organizational structure
*  Enterprise-wide initiatives that can be tied to GitLab
*  Clarity into an account's CE Usage Statistics
*  Insights found in an account's annual report
*  Contacts in account leadership positions and changes in leadership
*  Contacts responsible for particular aspects of the software development lifecycle, such as tools, phases, and methodologies
*  Direct dials on certain contacts
*  Planned speaking engagements for relevant contacts
*  Articles for reference

The Account Research Specialist will follow up to determine a final due date based upon number of tasks at hand and communicate to the SDR when the research has been completed.

The research conducted for SDRs should provide value by identifying information that can be used to get in contact with the right people faster, and account-specific information that can be used to improve the chance of securing an introductory meeting. A strong relationship with the Account Research Specialist, collaboration, and feedback will be essential in delivering useful results.

## Who to call

* Your SDR Team Lead will assign ~25-30 accounts per quarter in Salesforce from in the Strategic and Large segments. These target accounts will be provided by the SDR Team Lead, in close partnership with the relevant Regional Sales Director. The accounts will be identified and selected as the best accounts to focus on in order to meet our revenue objectives. The SDR Team Lead or Sales & Business Development Manager will add you to the SDR Field in Salesforce.
* If you inherit an account that was previously worked on by another SDR team member, please send a Chatter note to Courtland, Chet, Nick, JJ, or Francis as no one else has permission to reassign an SDR on an account.
* In the event that you feel your accounts are non-workable please consult with your SDR Team Lead. If they decide the account isn't workable, we will reassign all accounts and related contacts back to NULL, which will make them available for prospecting by other members of the SDR team.
* We use this [Salesorce report](https://na34.salesforce.com/00O61000003iZLP) to track and ensure proper coverage of Strategic and Large accounts and effectively use the SDR resources.

You should be looking for contacts within your accounts with IT leadership and application development leadership responsibilities. This can be senior technical individual contributors such as software architects, or leaders in the organization's IT department all the way up to a CIO or CTO.

## Compensation

* SDR’s compensation is based on two key metrics:
   * Sales Accepted Opportunities (SAOs)
   * Closed won business from SAOs - 1% commission for any closed won opportunity produced, **so as long as the rep is employed as an SDR by GitLab.

Your quota is based solely on SAOs. Think of the 1% commission on opportunities you source that a sales person subsequently closes and wins as an added bonus.

SDRs will be expected to get 2 SAOs Month 1, 4 SAOs Month 2, and 6 SAOs Month 3. After Month 3, SDRs are expected to get at least 6 SAOs per month.

There is an accelerator for SDRs who deliver over their SAO quota. The accelerator only applies to SAOs; the 1% commission on closed won business is not eligible. Your commission is calculated as follows:

* Tier 1 - Base rate: Per accepted opportunity commission is paid at a base rate of your monthly variable on target earnings divided by your monthly target.
* Tier 2 - Accelerator from 100% to 200% achievement: Each accepted opportunity after initial target is achieved will be paid at 1.05 times the base rate.
* Tier 3 - Accelerator from 200% achievment onwards: After 200% quota achievement each opportunity after will be paid at 1.1 times the base rate.
* There is no floor
* Payment tiers are progressive.

[Outbound SDR created opps](https://na34.salesforce.com/00O61000003nmhe) This is the report that GitLab leadership uses to see all the Outbound SDR created opportunities. If you believe an opportunity you sourced is not reflected in our reporting, notify your manager.

## Other measurements

SDR's will also be measured on the following:

* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of emails sent
  * Number of calls made

Note, while important, the above measurements do not impact your quota attainment. Your main focus will be achieving your SAO quota.

## Other expectations

* Attendance to the initial qualfiying meeting scheduled by the SDR is mandatory. The SDR should take notes during the initial qualifying meeting they set up for an AE or SAL.
* It will be in your best interest to sit in on as many meetings as possible at different stages in the buying process to see how the AEs and SALs work with prospects beyond qualifying. The better you understand the AEs and SALs you work with, the better you will be at making them successful through outbound prospecting support.

## Referring people to the SDR team

Know someone who works at a company with at least 750 technical employees? Please introduce them to a Sales Development Representative Lead. When an SDR receives an introduction, they will respectfully ask the person that was introduced to them if they would be willing to point them to any people who are responsible for evaluating or selecting SCM, CI/CD, application monitoring, and/or software project management solutions. This approach helps ensure we're talking to the right people, and helps reduce the time it takes to identify the people within a company who would be best suited to hear about how GitLab can help their organization ship better software, faster.

You can use the following text to make an introduction over email:

```
Hi {contact.first.name},

My colleague {sdr.lead.first.name} has been trying to find people responsible for evaluating and selecting SCM and CI/CD solutions at {contact.organization}. Could you do me a favor and point them in the right direction? They would be asking to schedule a quick call to discuss the possibility of using GitLab at {contact.organization}. Thank you, I appreciate your help.

Best,

{first.name}
```