---
layout: markdown_page
title: "Marketing Design System"
---

## Welcome to the Marketing Design System
{:.no_toc}

----

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

----

## Marketing Design System Handbooks
{:.no_toc}

- [Marketing Design System](/handbook/marketing/corporate-marketing/marketing-design-system/)  
- [Brand Guidelines](/handbook/marketing/corporate-marketing/marketing-design-system/brand-guidelines/)
- [Designer Onboarding](/handbook/designer-onboarding)

----


## Introduction

Welcome to the Marketing Design System; this resource exists to enable designers and non-designers alike to present a consistent and cohesive GitLab brand experience through brand resources, asset libraries, and templates.

This is a work-in-progress with the goal of being all-encompassing. Use the `marketing-design` Slack channel if you need help tracking anything down. Check back often for updates, create an issue in the [Marketing repo](https://gitlab.com/gitlab-com/marketing) for feedback, requests, and nice-to-haves.

### Brand resources

- [GitLab logos](https://gitlab.com/gitlab-com/marketing/tree/master/design/gitlab-brand-files/gitlab-logo) (web RGB & print CMYK)
- [GitLab wordmarks](https://gitlab.com/gitlab-com/marketing/tree/master/design/gitlab-brand-files/gitlab-wordmark) (web RGB & print CMYK)
- Print-ready [event one-pagers](https://gitlab.com/gitlab-com/marketing/tree/master/design/print/event-one-pagers)
- Color palette and Typography can be found in the [Brand Guidelines](/handbook/marketing/corporate-marketing/marketing-design-system/brand-guidelines/)
- [Authorized Reseller GitLab Virtuoso Badge](https://gitlab.com/gitlab-com/marketing/tree/master/design/gitlab-brand-files/authorized-resellers/gitlab-virtuoso-badge)

### Asset libraries

#### Icons

- [Line icons](https://gitlab.com/gitlab-com/marketing/tree/master/design/presentation-decks/_general-assets/icons/line-icons) (.png)
- [Line icons](https://gitlab.com/gitlab-com/marketing/tree/master/design/_resources/icons/svg) (.svg)
- [Icon illustrations](https://gitlab.com/gitlab-com/marketing/tree/master/design/presentation-decks/_general-assets/icons/illustrated-icons) (.png)
- [Software Development Lifecycle](https://gitlab.com/gitlab-com/marketing/tree/master/design/gitlab-brand-files/software-development-lifecycle/complete-lifecycle-icons/png)


#### Icon patterns

- [GitLab icon pattern](https://gitlab.com/gitlab-com/marketing/tree/master/design/gitlab-brand-files/illustrations/icon-pattern)

#### Social media

- [Profile assets](https://gitlab.com/gitlab-com/marketing/tree/master/design/social-media/profile-assets/png/assets)

### Templates

#### Presentation decks

- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [Functional Group Update template](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)
