---
layout: job_page
title: "Business Development Representative, EMEA"
---

{: .text-center}
<br>

You love talking about GitLab to people and no question or challenge is too big or small. You have experience working directly with customers in order to answer questions on getting started with a technical product. Your job is to make sure our customers are successful, from the single-shop development firm all the way up to our Fortune 500 customers, and that everyone gets the appropriate level of support.

## Responsibilities

* Strategize with our business development team lead to develop the proper qualifying questions for all types of customers. Be able to identify where a customer is in the sales and marketing funnel and take the appropriate action.
* Discretion to qualify and disqualify leads when appropriate.
* Develop and work with our business development team lead to follow and improve the [processes](https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/) for passing leads to sales, support, etc.
* Participate in documenting all processes in the handbook and update as needed with our business development team lead.
* Work closely with support to build a process where we can identify customers that would be interested in GitLab Enterprise Edition (EE) and pass those onto sales.
* Manage and help qualify and/or route inbound requests and inquiries.
* Collaborate with our marketing team on messaging for demand generation campaigns.
* Work to introduce more GitLab CE and GitLab.com users to our subbscription offerings.
* Work closely with sales to identify new competitive technologies and systems and understanding the drawbacks and benefits of EE.
* Work closely with our marketing team to identify customer stories from all of the conversations you have with our customers.

## Requirements

* While you need to serve customers in EMEA, you can work remotely from anywhere in the world
* Excellent spoken and written English
* Fluency in spoken and written German and/or other European languages is preferred.
* Experience in sales, marketing, or customer service
* Experience with CRM and/or marketing automation software is highly preferred
* An understanding of B2B software, Open Source software, and the developer product space is preferred
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Passionate about technology and learning more about GitLab
* Be ready to learn how to use GitLab and Git
* Start part-time or full-time depending on situation
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).


* Qualified applicants receive a short questionnaire from one of our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the BDR Team Lead
* Candidates will then be invited to schedule an interview with the Sales and Business Development Manager
* Candidates will be invited to schedule a third interview with our Senior Director, Marketing & Sales Development
* Finally, candidates may be asked to interview with our CEO
* Successful candidates will subsequently be made an offer via email


Additional details about our process can be found on our [hiring page](/handbook/hiring)

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).



