---
layout: job_page
title: "Director of Security"
---

The Director of Security [reports to the VP of Engineering](/team/structure/).

Our thesis is that [Good Security Is Holistic](https://medium.com/@justin.schuh/stop-buying-bad-security-prescriptions-f18e4f61ba9e).
We think that simulating a security culture in engineering is one of the most
important things. We don't do checklist security, the goal is to keep the trust
of our users by being secure, compliance is not a goal in itself. We don't think
that third party products are unimportant but they are not a silver bullet to
making everything secure.

As Director of Security at GitLab, you will succeed if you enable, enact, and
evangelize such holistic security around the application, GitLab.com, and the
organization as a whole such that GitLab and GitLab.com are established as being
 at the vanguard of security thinking and practices.

## Responsibilities

- Secure our products (GitLab CE/EE), services (GitLab.com, package servers, other infrastructure), and company (laptops, email).
- Keep our risk analysis up to date.
- Define and plan priorities for security related activities based on that risk analysis.
- Determine appropriate combination of internal security efforts and external
security efforts including bug bounty programs, external security audits
(penetration testing, black box, white box testing).
- Analyze and advise on new security technologies.
- Build and manage a team, which currently consists of our [Security Lead](https://about.gitlab.com/jobs/security-lead)
and [Security Specialists](https://about.gitlab.com/jobs/security-specialist)(vacancy).
   - Identify and fill positions.
   - Grow skills in team leads and individual contributors, for example by
   creating training and testing materials.
   - Deliver input on promotions, function changes, demotions, and terminations.
- Ensure our engineers and contributors from the wider community run a secure software development lifecycle for GitLab by training them in best practices and creating automated tools.
- Respond to security and service abuse incidents.
- Perform red team security testing of our product and infrastructure.
- Run our bounty program effectively.
- Ensure we're compliant with our legal and contractual security obligations.

## Requirements

- Significant application and SaaS security experience in production-level settings.
- This position does not require extensive development experience but the
applicant should be very familiar with common security libraries, security
controls, and common security flaws that apply to Ruby on Rails applications.
- Experience managing teams of engineers, and leading managers.
- Experience with (managing) incident response.
- You share our [values](/handbook/values), and work in accordance with those values.

### Hiring Process

The hiring process for this role consists of _at least_

- [screening call](/handbook/hiring/#screening-call)
- interview with Security Lead
- interview with Director of Infrastructure
- interview with VP of Engineering
- interview with CEO

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
